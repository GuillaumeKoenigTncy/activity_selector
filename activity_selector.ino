/*********
  Rui Santos
  Complete project details at https://randomnerdtutorials.com  
*********/

//        digitalWrite(LED_BUILTIN, HIGH);
//        delay(1000);
//        digitalWrite(LED_BUILTIN, LOW);
//        delay(1000);    
//        digitalWrite(LED_BUILTIN, HIGH);
//        delay(900);
//        digitalWrite(LED_BUILTIN, LOW);
//        delay(900);
//        digitalWrite(LED_BUILTIN, HIGH);
//        delay(800);
//        digitalWrite(LED_BUILTIN, LOW);
//        delay(800);
//        digitalWrite(LED_BUILTIN, HIGH);
//        delay(700);
//        digitalWrite(LED_BUILTIN, LOW);
//        delay(700);
//        digitalWrite(LED_BUILTIN, HIGH);
//        delay(600);
//        digitalWrite(LED_BUILTIN, LOW);
//        delay(600);
//        digitalWrite(LED_BUILTIN, HIGH);
//        delay(500);
//        digitalWrite(LED_BUILTIN, LOW);
//        delay(500);
//        digitalWrite(LED_BUILTIN, HIGH);
//        delay(400);
//        digitalWrite(LED_BUILTIN, LOW);
//        delay(400);
//        digitalWrite(LED_BUILTIN, HIGH);
//        delay(300);
//        digitalWrite(LED_BUILTIN, LOW);
//        delay(300);
//        digitalWrite(LED_BUILTIN, HIGH);
//        delay(250);
//        digitalWrite(LED_BUILTIN, LOW);
//        delay(250);
//        digitalWrite(LED_BUILTIN, HIGH);
//        delay(200);
//        digitalWrite(LED_BUILTIN, LOW);
//        delay(200);
//        digitalWrite(LED_BUILTIN, HIGH);
//        delay(150);
//        digitalWrite(LED_BUILTIN, LOW);
//        delay(150);
//        digitalWrite(LED_BUILTIN, HIGH);
//        delay(100);
//        digitalWrite(LED_BUILTIN, LOW);
//        delay(100);
//        digitalWrite(LED_BUILTIN, HIGH);
//        delay(75);
//        digitalWrite(LED_BUILTIN, LOW);
//        delay(75);
//        digitalWrite(LED_BUILTIN, HIGH);
//        delay(50);
//        digitalWrite(LED_BUILTIN, LOW);
//        delay(50);
//        digitalWrite(LED_BUILTIN, HIGH);
//        delay(25);
//        digitalWrite(LED_BUILTIN, LOW);
//        delay(25);


#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 32 // OLED display height, in pixels

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);

int i = 1000;

char const *myActivities[] = {"Cinema au Cinema", "Cinema a la maison", "Marche en ville !", "Rstaurant dehors !", "Repas romantique",
"Cuisiner un gateau", "Shopping", "Balade a l'aquarium", "Marche en foret !", "Boire un verre au bar", "Marche au parc !","Faire un légo a 2"};

char const *myFood[] = {"McDonald", "KFC", "Burger King", "Pokawa", "Poutine", "Jap'", "(M) Pates", "(M) Salade"};

// constants won't change. They're used here to set pin numbers:
const int buttonPin = 2;  // the number of the pushbutton pin
const int choicePin = 3;  // the number of the pushbutton pin
const int ledPin = 13;    // the number of the LED pin

// variables will change:
int buttonState = 0;  // variable for reading the pushbutton status

int lastButtonStateCode = 0;

void setup() {
  Serial.begin(115200);

  // initialize the LED pin as an output:
  pinMode(ledPin, OUTPUT);
  // initialize the pushbutton pin as an input:
  pinMode(buttonPin, INPUT);
  pinMode(choicePin, INPUT);

  pinMode(LED_BUILTIN, OUTPUT);

  if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { // Address 0x3D for 128x64
    Serial.println(F("SSD1306 allocation failed"));
    for(;;);
  }
  delay(1000);
  
  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0, 15);
  display.println("Hello, world!");
  display.display();
  delay(1000);

}

void loop() {

  int randNumber = random(0, 10);

  if (digitalRead(choicePin)) {
    int buttonStateCode = digitalRead(buttonPin);
    if (buttonStateCode != lastButtonStateCode) {
      if (buttonStateCode == 1) {
        
          digitalWrite(LED_BUILTIN, LOW);
          
          digitalWrite(LED_BUILTIN, HIGH);
          
          display.clearDisplay();
          display.setCursor(0, 15);
          display.print(myActivities[randNumber]);
          display.display(); 
      }
      lastButtonStateCode = buttonStateCode;
    }
  } else {
    int buttonStateCode = digitalRead(buttonPin);
    if (buttonStateCode != lastButtonStateCode) {
      if (buttonStateCode == 1) {
        
          digitalWrite(LED_BUILTIN, LOW);
          
          digitalWrite(LED_BUILTIN, HIGH);
          
          display.clearDisplay();
          display.setCursor(0, 15);
          display.print(myFood[randNumber]);
          display.display(); 
      }
      lastButtonStateCode = buttonStateCode;
    }
  }
  

}
